#!/usr/bin/env python3
import sys
from typing import Iterable, List

import setuptools
import setuptools.command.test


def get_requirements(filename: str) -> Iterable[str]:
    """Read requirements from file."""
    with open(filename, mode="rt", encoding="utf-8") as f:
        for line in f:
            # TODO: Will not work with #egg-info
            requirement = line.partition("#")[0].strip()
            if not requirement:
                continue
            yield requirement


def get_textfile(filename: str) -> str:
    """Get contents from a text file."""
    with open(filename, mode="rt", encoding="utf-8") as f:
        return f.read().lstrip()


def get_packages() -> List[str]:
    """List of (sub)packages to install."""
    return setuptools.find_packages(".", include=("fsfd_client", "fsfd_client.*"))


class PyTest(setuptools.command.test.test):
    """Run tests using pytest.

    From `http://doc.pytest.org/en/latest/goodpractices.html`.

    """

    user_options = [("pytest-args=", "a", "Arguments to pass to pytest")]

    def initialize_options(self) -> None:
        super().initialize_options()
        self.pytest_args: List[str] = []

    def run_tests(self) -> None:
        import shlex
        import pytest

        args = self.pytest_args
        if args:
            args = shlex.split(args)  # type: ignore[arg-type]
        errno = pytest.main(args)
        raise SystemExit(errno)


def run_setup() -> None:
    setup_requirements = ["setuptools_scm"]
    test_requirements = list(get_requirements("requirements-test.txt"))
    install_requirements = list(get_requirements("requirements.txt"))

    if {"build_sphinx", "upload_docs"}.intersection(sys.argv):
        setup_requirements.extend(get_requirements("docs/requirements.txt"))
        setup_requirements.extend(install_requirements)

    setuptools.setup(
        name="fsfd-client",
        description="Client for the FSFD-",
        long_description=get_textfile("README.md"),
        long_description_content_type="text/markdown",
        url="https://git.app.uib.no/it-bott-integrasjoner/fsfd-client",
        author="BOTT-INT",
        # TODO: Add proper author_email
        author_email="bnt-int@usit.uio.no",
        use_scm_version=True,
        packages=get_packages(),
        setup_requires=setup_requirements,
        install_requires=install_requirements,
        tests_require=test_requirements,
        cmdclass={
            "test": PyTest,
        },
        classifiers=[
            "Development Status :: 3 - Alpha",
            "Intended Audience :: Developers",
            "Topic :: Software Development :: Libraries",
            "Programming Language :: Python :: 3 :: Only",
            "Programming Language :: Python :: 3.6",
            "Programming Language :: Python :: 3.7",
            # TODO: Include 'Programming Language :: Python :: 3.8', ?
        ],
        keywords="FSFD API client",
    )


if __name__ == "__main__":
    run_setup()
