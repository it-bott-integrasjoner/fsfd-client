# fsfd-client

# Client for the FSFD-api

## Example:

```
from fsfd_client.client import get_client

config_dict = {
    "base_url": "https://www.example.com/",
    "distlist_medlemmer_url": "distlist_medlemmer",
    "kontakter_url": "kontakter",
    "headers": {
        "X-Gravitee-Api-Key": "..."
    },
}

client = get_client(config_dict)
data = client.get_dist_lists()
print(data)

```