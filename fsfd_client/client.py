"""Client for the FSFD-api"""

import typing

import requests


class FsfdClient:
    """Client"""

    def __init__(
        self,
        base_url: str,
        distlist_medlemmer_url: str,
        kontakter_url: str,
        headers: typing.Dict[str, str],
    ):
        self.base_url = base_url
        self.distlist_medlemmer_url = distlist_medlemmer_url
        self.kontakter_url = kontakter_url
        self.headers = headers

    def get_dist_lists(self) -> typing.Any:
        """Get dist lists"""
        url = self.base_url + self.distlist_medlemmer_url
        result = requests.get(url, headers=self.headers, timeout=3600)
        result_json = result.json()
        return result_json["distlist_medlem"]

    def get_contacts(self) -> typing.Any:
        """Get contacts"""
        url = self.base_url + self.kontakter_url
        result = requests.get(url, headers=self.headers, timeout=3600)
        result_json = result.json()
        return result_json["Kontakt"]


def get_client(config_dict: typing.Dict[str, typing.Any]) -> FsfdClient:
    """
    Get a FsfdClient from configuration.
    """
    return FsfdClient(**config_dict)
