def test_get_dist_lists(client, base_url, requests_mock, distlist_data):
    requests_mock.get(
        "https://example.com/distlist_medlemmer",
        json=distlist_data,
    )
    received = client.get_dist_lists()
    expected = distlist_data["distlist_medlem"]
    assert len(expected) == len(received)


def test_get_contacts(client, base_url, requests_mock, contact_data):
    requests_mock.get(
        "https://example.com/kontakter",
        json=contact_data,
    )
    received = client.get_contacts()
    expected = contact_data["Kontakt"]
    assert len(expected) == len(received)
