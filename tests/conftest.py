import json
import os

import pytest

from fsfd_client.client import get_client


def load_json_file(name):
    here = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    with open(os.path.join(here, "fixtures", name)) as f:
        data = json.load(f)
    return data


@pytest.fixture
def base_url():
    return "https://example.com/"


@pytest.fixture
def config(base_url):
    return {
        "base_url": base_url,
        "distlist_medlemmer_url": "distlist_medlemmer",
        "kontakter_url": "kontakter",
        "headers": {},
    }


@pytest.fixture
def client(config):
    return get_client(config)


@pytest.fixture
def distlist_data():
    return load_json_file("distlist.json")


@pytest.fixture
def contact_data():
    return load_json_file("contact.json")
